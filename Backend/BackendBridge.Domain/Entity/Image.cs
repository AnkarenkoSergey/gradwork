﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackendBridge.Domain.Entity
{
    public class Image
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Data { get; set; }
        
    }
}
